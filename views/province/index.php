<?php
    require '../views/header.php';
?>
<div id="content">
    <h1>Lista de provincias</h1>

    <p><a href="/province/seek">Buscar Provincia</a></p>
    <table>
        <thead>
        <tr>
            <th>Id</th>
            <th>Provincia</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($rows as $row) : ?>
        <tr>
            <td><?php echo $row['id'] ?></td>
            <td><?php echo $row['provincia'] ?></td>
            <td> 
             <a href="/province/remember/<?php echo $row['provincia'] ?>">Recordar</a>
             <a href="/province/delete/<?php echo $row['id'] ?>">Borrar</a>
             </td>
        </tr>            
        <?php endforeach ?>
        </tbody>

    </table>

    <?php for ($i = 1; $i <=6; $i++) {?>
        <a href="/province/index/<?php echo $i ?>"> <?php echo $i ?> </a>
         - 

    <?php } ?>
</div>

<?php
    require '../views/footer.php';
?>
